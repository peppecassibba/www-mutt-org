-----BEGIN PGP SIGNED MESSAGE-----

		  M U T T   1 . 0   I S    O U T .


After several years of development and more or less well-working
alpha and beta versions, we proudly present version 1.0 of the Mutt
mail user agent.

Mutt is a powerful text-based e-mail client for Unix operating
systems, distributed under the GNU General Public License.

Some bullet points about mutt's features and properties:

    * color support
    * message threading
    * MIME support (including RFC2047 support for encoded headers)
    * PGP/MIME (RFC2015)
    * various features to support mailing lists, including list-reply
    * active development community
    * POP3 support
    * IMAP support
    * full control of message headers when composing
    * support for multiple mailbox formats (mbox, MMDF, MH, maildir)
    * highly customizable, including keybindings and macros 
    * change configuration automatically based on recipients, current
      folder, etc.
    * searches using regular expressions, including an internal pattern
      matching language
    * postpone message composition indefinetly for later recall
    * easily include attachments when composing, even from the command
      line
    * ability to specify alternate addresses for recognition of
      mail forwarded from other accounts, with ability to set
      the From: headers on replies/etc. accordingly
    * multiple message tagging
    * reply to or forward multiple messages at once
    * .mailrc style configuration files
    * easy to install (uses GNU autoconf)
    * complies against either curses/ncurses or S-lang
    * efficient

For information on the web, please see http://www.mutt.org/.


Mutt (which was originally the work of Michael Elkins <me@mutt.org>)
has been the work of several dozen contributors.



			Download Information


Mutt is distributed in source form.  You'll find two versions in the
FTP archives: A 1.0i version which contains support for composing
and receiving PGP-encrypted and -signed messages, and a 1.0 vesion
without any PGP support.

The source files are PGP-signed with a well-certified key.  For
reference, we include MD5 checksums of the distribution tar balls:

	5f3072b7edd78073a5bc2e42222eb66b  mutt-1.0.tar.gz
	88e825e6fed4e5d83989d16f7d8a2505  mutt-1.0i.tar.gz

You can download Mutt from the following FTP locations:

    * ftp://ftp.mutt.org/pub/mutt/ (primary site)
    * ftp://ftp.guug.de/pub/mutt/ (same machine, different host name)

Mirror sites (in alphabetical order):

    * ftp://ftp.42.org/pub/unix/mail/mutt/
    * ftp://ftp.arch.pwr.wroc.pl/pub/mutt/
    * ftp://ftp.cdrom.com/pub/unixfreeware/email/mutt/
    * ftp://ftp.demon.co.uk/pub/mirrors/mutt/
    * ftp://ftp.fu-berlin.de/pub/unix/mail/mutt/
    * ftp://ftp.funet.fi/pub/unix/mail/mutt/
    * ftp://ftp.gbnet.net/pub/mutt-international/
    * ftp://ftp.gwdg.de/pub/unix/mail/mutt/international/
    * ftp://ftp.iks-jena.de/pub/mitarb/lutz/crypt/software/pgp/mutt/
    * ftp://ftp.is.co.za/networking/mail/mua/mutt/
    * ftp://ftp.jp.qmail.org/mutt/ (Japanese)
    * ftp://ftp.kfki.hu/pub/packages/mail/mutt/
    * ftp://ftp.linux.it/pub/mirrors/mutt/
    * ftp://ftp.lip6.fr/pub/unix/mail/mutt/
    * ftp://ftp.medasys-digital-systems.fr/pub/unix/mutt/
    * ftp://ftp.ntua.gr/pub/net/mail/mutt/
    * ftp://ftp.spyda.net/pub/mutt/
    * ftp://ftp.uib.no/pub/mutt/
    * ftp://gd.tuwien.ac.at/infosys/mail/mutt/
    * ftp://pgp.rasip.fer.hr/pub/mutt/
    * ftp://riemann.iam.uni-bonn.de/pub/mutt/
    * ftp://sunsite.uio.no/pub/mail/mutt/
    * ftp://uiarchive.cso.uiuc.edu/pub/packages/mail/mutt/

Note that it may take a day or two for the new version of mutt to
propagate to all these mirror sites.


-----BEGIN PGP SIGNATURE-----
Version: 2.6.3in
Charset: latin1

iQEVAwUBOBAcI9ImKUTOasbBAQF8kwf/a4gWOXhVRzyC/hRlH7ANcrUgw8BZNLub
fLVh+sKkJKHX56Pur3JNyx+prF1CLphwa2A8mVdXbNup36p+VnYL2N8fvFyZ2qUL
VpMXy8xnQPtnh9kp/2BpzuvMsYjf3kyBJPReAnhO2ALvp1z3B248lSjYFkz4kFXM
NVTE4bztz9khWL1WcygHRJNfr8sSQvi2rzjVbeDLC6DE5PAeUabj2gf1coRtXDn2
IYb64elh1P1CkzbwQGhiAwtQIfNwtWkfR6E67qKSoA0tY/JTyxBtFiJoh/AxB8vn
AayvieX0CkVoGXImkmMXF3hBc0yxCSrwV+LLCt3HJpyZ6qRIcKEdkw==
=JejV
-----END PGP SIGNATURE-----
