<!DOCTYPE html>
<html>
  <head>
    <title>Mutt 1.13 Release Notes</title>
  </head>

  <body>
    <h1>Mutt 1.13 Release Notes</h1>
    <p>
      Note: this is a review of some of the more interesting features
      in this release.  For a complete list of changes please be sure
      to read the
      <a href="https://gitlab.com/muttmua/mutt/raw/stable/UPDATING">UPDATING</a>
      file.
    </p>

    <h2>Autocrypt</h2>
    <p>
      <a href="https://autocrypt.org/">Autocrypt</a> provides easy to
      use, passive protection against data collection.  It exchanges
      keys via email headers, and though doesn't protect against
      active adversaries, is still an easy way to minimize clear-text
      emails sent between common correspondants without having to
      explicitly exchange keys or deal with the details of OpenPGP.
    </p>

    <p>
      Before enabling autocrypt in Mutt, <b>please</b> take the time
      to read the
      <a href="/doc/manual/#autocryptdoc">autocrypt section</a> in the
      manual.  By default, Mutt generates a new ECC key, and stores
      keys in a separate keyring.  It's important to understand how it
      works and what's going on behind the scenes.
    </p>

    <p>
      When enabled, a new line will appear in the compose menu,
      containing the autocrypt status, and output of the
      recommendation engine:
    </p>

    <p>
      <img src="autocrypt-compose.png">
    </p>

    <p>
      If the recommendation engine returns &quot;Yes&quot; and normal
      encryption/signing is off, autocrypt will automatically be
      enabled, as seen above.  It's important to note
      that <b>anything</b> enabling normal encryption or signing will
      override autocrypt encryption.  You can still see the status of
      the recommendation engine, but it won't automatically turn on
      autocrypt if normal encryption/signing is enabled:
    </p>

    <p>
      <img src="autocrypt-compose-2.png">
    </p>

    <p>
      Autocrypt can be manually enabled, disabled, or reset to
      automatic mode via the &lt;autocrypt-menu&gt; function, by
      default bound to &quot;o&quot;:
    </p>

    <p>
      <img src="autocrypt-compose-3.png">
    </p>

    <h2>Browser Sticky Cursor</h2>
    <p>
      <a href="/doc/manual/#browser-sticky-cursor">$browser_sticky_cursor</a>,
      set by default, tries to keep the cursor on the same mailbox
      across various operations (such as changing to a parent folder,
      modifying the mask, and creating directories).
    </p>

    <p>
      It also tries to place the cursor on the currently open mailbox
      when opening the browser.  A new function to browse mailboxes
      &lt;browse-mailboxes&gt; was added, and is bound by default to
      &quot;y&quot;.  This should replace any macros such as
      &quot;&lt;change-folder&gt;?&lt;toggle-mailboxes&gt;&quot;, as
      it ensures the current mailbox is selected.  (The macro suffers
      from a problem if the initial directory listing doesn't contain
      the currently open mailbox.)
    </p>

    <h2>Multipart/Alternative Generation</h2>
    <p>
      This release provides a way to generate a single
      multipart/alternative part via an external filter script.
      <a href="/doc/manual/#send-multipart-alternative">$send_multipart_alternative</a>
      is a quadoption enabling the filter, and the filter itself is
      specified via
      <a href="/doc/manual/#send-multipart-alternative-filter">$send_multipart_alternative_filter</a>.
      It may also be useful to set these variables via send or reply
      hooks, to control when and how alternatives are used.
    </p>

    <p>
      The composed part (usually text/plain) will be piped into the
      filter.  The output of the filter should be the generated mime
      type (e.g. &quot;text/html&quot;), a blank line, and the
      generated output.  Note that the filter isn't allowed to
      generate a multipart type itself (due to current technical
      limitations).
    </p>

    <p>
      The output of the filter can be previewed in the compose menu
      via the functions &lt;view-alt&gt;, &lt;view-alt-text&gt;
      and &lt;view-alt-mailcap&gt, bound to "v", "Esc v" and "V" by
      default.
    </p>

    <p>
      A sample filter has been included in contrib/markdown2html.
    </p>

    <h2>Sidebar Indentation Changes</h2>
    <p>
      The
      <a href="/doc/manual/#sidebar-folder-indent">$sidebar_folder_indent</a>
      and <a href="/doc/manual/#sidebar-short-path">$sidebar_short_path</a>
      options have been changed to operate based on the preceding
      entries displayed in the sidebar.  Indentation will be one more
      than the most recent greatest common path mailbox, and will be
      shortened based on that too.
    </p>

    <p>
      Additionally, mailboxes are now prefixed with &quot;~&quot; or
      &quot;=&quot; in the same manner as other places in Mutt (such
      as the status bar, or in the browser menu).
    </p>

    <p>
      <b>Note</b>: the above two changes were originally by default in
      1.13.0.  Release 1.13.1 disabled by these by default and added
      two new configuration options to re-enable them:
      <a href="/doc/manual/#sidebar-relative-shortpath-indent">$sidebar_relative_shortpath_indent</a> and
      <a href="/doc/manual/#sidebar-use-mailbox-shortcuts">$sidebar_use_mailbox_shortcuts</a>.
    </p>


    <h2>Smaller Features/Changes</h2>

    <h3>$write_bcc unset</h3>
    <p>
      <a href="/doc/manual/#write-bcc">$write_bcc</a> has been changed
      to default unset.  Several MTAs do not strip Bcc headers, making
      it dangerous to have this set by default.  Sorry everyone, had I
      been aware of this issue before, I would have changed this
      earlier.
    </p>
    <p>
      In order for your Fcc'ed copy to be accurate, I've disabled the
      option's effect on the Fcc'ed message.  That will always have
      the Bcc header; only the sent message will be affected by
      $write_bcc.
    </p>

    <h3>Regex configuration rename</h3>
    <p>
      &quot;--with-regex&quot; has been renamed to
      &quot;--with-bundled-regex&quot;.  The previous name incorrectly
      implied it was necessary for regex support.  Almost all modern systems
      contain a suitable regex library, but if not the bundled regex can be
      used as a backup.
    </p>

    <p>
      Additionally commit
      <a href="https://gitlab.com/muttmua/mutt/commit/367b1135ff1a74fe047396d940716d28748f8935">367b1135</a>
      fixed undefined left-shift on negative value behavior in the
      bundled regex.c library, in case anyone was losing sleep over that. ;-)
    </p>

    <h3>SSL changes</h3>
    <p>
      <a href="/doc/manual/#ssl-force-tls">$ssl_force_tls</a> now defaults set.
      If you are connecting to a local server over clear text, be aware that
      you will need to unset it.
    </p>

    <p>
      <a href="/doc/manual/#ssl-use-tlsv1-3">$ssl_use_tlsv1_3</a> was also
      added, default set.
    </p>

    <h3>User Agent off by default</h3>
    <p>
      In response to repeated requests by the security minded,
      <a href="/doc/manual/#user-agent">$user_agent</a> is default
      unset.  However, for those who wish to show their Mutt Love,
      you can still enable this yourself.
    </p>

    <h3>Attachment counting</h3>
    <p>
      The <a href="/doc/manual/#attachments">unattachments</a> command
      now accepts &quot;*&quot; to remove all attachment counting
      entries.
    </p>

    <p>
      Also, <a href="/doc/manual/#count-alternatives">$count_alternatives</a>
      has been added to scan inside multipart/alternatives for
      matching attachments.  Traditionally attachments are not
      supposed to be there, but some crazy mail clients decide to put
      them in one of the alternatives.  Set this to find and count
      those too.
    </p>

    <h3>Byte size display control</h3>
    <p>
      <a href="/doc/manual/#size-show-bytes">$size_show_bytes</a>,
      <a href="/doc/manual/#size-show-mb">$size_show_mb</a>,
      <a href="/doc/manual/#size-show-fractions">$size_show_fractions</a>, and
      <a href="/doc/manual/#size-unites-on-left">$size_units_on_left</a>
      have been added to control how bytes are displayed in various
      format string expandos.
      See <a href="/doc/manual/#formatstrings-size">Bytes size
      display</a> in the manual for more details.
    </p>

    <h3>format=flowed space stuffing</h3>
    <p>
      Only the most observant will have noticed, but format=flowed
      space stuffing has been accidentally disabled in Mutt for
      several years.  Commit
      <a href="https://gitlab.com/muttmua/mutt/commit/4959e8d4b43f89911a734da8c9bbb5d28227c317">4959e8d4</a>
      reenabled this.  It also changed Mutt to automatically
      unstuff/stuff around every message editing operation, as opposed
      to only after the first edit.
    </p>

    <h2>Internal Improvements</h2>
    <p>
      A lot of internal improvements happened during this development
      cycle too:
      <ul>
        <li>
          IMAP new mail checking was made more robust responding to
          new mail and reopen events.
        </li>
        <li>
          The data structures used to hold all config variables and commands
          had some portability issues straightened out.
        </li>
        <li>
          More work toward transitioning to dynamic string buffers took place.
          There is still more to do however...
        </li>
      </ul>
    </p>
  </body>
</html>
